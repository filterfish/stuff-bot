# coding: utf-8
Gem::Specification.new do |spec|
  spec.name          = "stuff-bot"
  spec.version       = "1.5"
  spec.authors       = ["Richard Heycock"]
  spec.email         = ["rgh@filterfish.org"]
  spec.summary       = "Stuff Bot"
  spec.description   = "Bot to categorise stuff"
  spec.homepage      = "https://gitlab.com/filterfish/stuff-bot"
  spec.license       = "GPL-3.0"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }

  spec.add_runtime_dependency "git", "~> 1.5"
  spec.add_runtime_dependency "matrix_sdk", "~> 2"
end
