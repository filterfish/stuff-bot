Simple bot to write an org-mode file to a git repo

This bot matches on the first two characters, the first of which must be
a '.' and the second is based on which file you want the message to be
added to. If there is no match the file is added to 'general.org'.

To use this you will need to create a repo (it's currently hard coded but I'll
change that soon). To set it up run the following:

```bash
git init <dir>
cd <dir>
git c --allow-empty -m 'Repo for Stuff Bot'
git branch stuff-bot
```

Those commands create a new repo with a single empty commit and a branch from
that empty commit. I suppose I could've just commit'ed to master but It Seemed
Like a Good Idea at the Time™.
